import {render, html} from "lit-html";
import {setTodo} from "../idb";

export default class Detail {
    constructor(page, data) {
        this.page = page;
        this.properties = {
            todo: data
        };

        this.renderView()
    }

    template() {
        return html`
            <section class="h-full">
                <div>
                  <header>
                    <h1 class="mt-2 px-4 text-xl">Detail </h1>
                  </header>
                  <main class="todolist px-4 pb-20">
                    <div>
                        <div> Titre :</div>
                        <input style="border: 1px solid #333; width: 100%;" type="text" name="title" value="${this.properties.todo.title}">
                    </div>
                    <div>
                        <div> Description :</div>
                        <textarea style="border: 1px solid #333; width: 100%;" name="description">${this.properties.todo.description}</textarea>
                    </div>
                  </main>
                </div>

                <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
                  <form @submit="${this.handleForm.bind(this)}" class="w-full h-full flex justify-between items-center px-4 py-3">
                    <button
                      style="width: 100%"
                      aria-label="Add"
                      class="ml-4 rounded-lg text-uppercase bg-heraku h-full text-center px-3 uppercase text-white font-bold flex justify-center items-center"
                      type="submit">Update Task<lit-icon class="ml-2" icon="send"></lit-icon></button>
                  </form>  
                </footer>
            </section>
        `
    }

    renderView() {
        const view = this.template();
        render(view, this.page);
    }

    handleForm(e) {
        e.preventDefault();
        let title = document.querySelector('input[name="title"]').value;
        let description = document.querySelector('textarea[name="description"]').value;

        this.properties.todo.title = title;
        this.properties.todo.description = description;

        setTodo(this.properties.todo).then(
            res => console.log(res)
        );

        alert('Item update');
        document.location.href = "/";
    }
}



