import { html, render } from 'lit-html';
import { setTodos, getTodos, unsetTodo } from '../idb.js';
import { createTodo, deleteTodo } from '../api/todos.js';

export default class Home {
  constructor(page, data) {
    this.page = page;
    this.properties = {
      todos: data,
      title: ''
    };

    this.renderView();
  }

  set todos(value) {
    this.properties.todos = value;
  }

  get todos() {
    return this.properties.todos;
  }


  template() {
    return html`
      <section class="h-full">
        <div ?hidden="${!this.properties.todos.length}">
          <header>
            <h1 class="mt-2 px-4 text-xl">My awesome todos : </h1>
          </header>
          <main class="todolist px-4 pb-20">
            <ul>
              ${this.properties.todos.map(todo => html`
                 <div style="display: flex;">
                    <li class="todoItems" name="${todo.id}" style="width: 100%;"> ${todo.title}</li>
                    <button  name="${todo.id}" class="deleteBtn">X</button> 
                </div>
                `
              )}
            </ul>
          </main>
        </div>
        <div class="mt-8" ?hidden="${this.properties.todos.length}">
          <img class="object-contain px-20" src="/img/nodata.svg" alt="No data">
          <p class="mt-4 text-center text-xl">No todos yet, try to create a new one</p>
        </div>
        <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
          <form @submit="${this.handleForm.bind(this)}" class="w-full h-full flex justify-between items-center px-4 py-3">
            <label class="flex-1" aria-label="Add todo input">
              <input
                autocomplete="off"
                .value="${this.properties.title}"
                @input="${e => this.properties.title = e.target.value}"
                class="py-3 px-4 rounded-sm w-full h-full"
                type="text"
                placeholder="Enter a new todo ..."
                name="todo">
            </label>
            <button
              aria-label="Add"
              class="ml-4 rounded-lg text-uppercase bg-heraku h-full text-center px-3 uppercase text-white font-bold flex justify-center items-center"
              type="submit">Add<lit-icon class="ml-2" icon="send"></lit-icon></button>
          </form>  
        </footer>
      </section>
    `;
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
    let deleteBtn = document.getElementsByClassName('deleteBtn');

    for (let i = 0; i < deleteBtn.length; i++) {
      let name = deleteBtn.item(i).name;
      deleteBtn.item(i).addEventListener('click', () => {
        this.todoDelete(name);
      })
    }

    let todoItems = document.getElementsByClassName('todoItems');

    for (let i = 0; i < todoItems.length; i++) {
      let name = todoItems.item(i).getAttribute('name');
      todoItems.item(i).addEventListener('click', () => {
        this.handleEdit(name);
      });
    }

  }

  todoDelete(id){
    let todo = this.todos.find( todo => todo.id = id);
    let index = this.todos.indexOf(todo);

    deleteTodo(id)
        .then( res => console.log("Suppress from database"))
        .catch(error => { console.log(error)});

    unsetTodo(parseInt(id))
        .then( res =>
            console.log("Suppress from local Storage")
        )
        .catch(error => {console.log(error)});

    this.todos.splice(index, 1);
    this.renderView();
  }

  handleForm(e) {
    e.preventDefault();
    console.log(this.properties.title);
    let todo = {
      id: Date.now(),
      title: this.properties.title,
      sync: 'false'
    }

    let msg;
    const create = createTodo(todo)
       .then( create =>  {
          if(create !== false) {
            console.log("Item store item in database");
            todo.sync = "true";
            msg = "Store with synchronization";
          } else {
            console.log("Failed to store item in database");
            msg = "Store but not synchronized";
          }
        });
    this.properties.todos = [...this.properties.todos, todo];

    setTodos(this.properties.todos)
        .then( todo => console.log("Store in IdB"));

    this.renderView();
  }

  handleEdit(id){
    document.location.href = "/" + id;
  }

}
