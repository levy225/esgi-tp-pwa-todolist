import { openDB } from 'idb';

export async function initDB() {
  const config = window.config;
  const db = await openDB('awesome-todo', config.version, {
    upgrade(db) {
      const store = db.createObjectStore('todos', {
        keyPath: 'id'
      });
      store.createIndex('id', 'id');
      store.createIndex('title', 'title');
      store.createIndex('description', 'description');
      store.createIndex('sync', 'sync');
    }
  });
  return db;
}

export async function setTodos(data) {
  const db = await initDB();
  const tx = db.transaction('todos', 'readwrite');
  data.forEach(item => {
    tx.store.put(item);
  });
  await tx.done;
  return await db.getAllFromIndex('todos', 'title');
}


export async function getTodos() {
  const db = await initDB();
  return await db.getAllFromIndex('todos', 'title');
}

export async function getTodo(id) {
  const db = await initDB();
  return  await db.get('todos', Number(id));
}

export async function setTodo(data) {
  const db = await initDB();
  const tx = db.transaction('todos', 'readwrite');
  await tx.store.put(data);
  return await db.get('todos', Number(data.id));
}


export async function unsetTodo(id) {
  const db = await initDB();
  await db.delete('todos', id);
  return await db.getAllFromIndex('todos', 'title', 'false');
}